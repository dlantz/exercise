if __name__ == '__main__':
    file = open("inputs/15.txt", "r")
    numbers = list(map(int, file.readline().split(',')))
    last_occurrence = {}

    for i, n in enumerate(numbers[:-1]):
        last_occurrence[n] = i

    last_spoken = numbers[-1]
    for i in range(len(numbers), 30000000):
        last_spoken_index = i - 1
        if last_spoken not in last_occurrence:
            last_occurrence[last_spoken] = last_spoken_index
            last_spoken = 0
        else:
            last_occurrence[last_spoken], last_spoken = \
                last_spoken_index, last_spoken_index - \
                last_occurrence[last_spoken]

    print(last_spoken)
