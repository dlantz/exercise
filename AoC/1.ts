import { readLines } from "./deps.ts";

async function main() {
  const numbers = [];
  for await (const line of readLines(Deno.stdin)) {
    numbers.push(parseInt(line));
  }
  // const output = findTwoSome(2020, numbers);
  // PART 2
  const output = findThreesome(2020, numbers);
  return output.reduce((a, b) => a * b);
}

const findTwoSome = (target: number, numbers: number[]): number[] => {
  for (const number of numbers) {
    if (numbers.includes(target - number)) {
      return [number, target - number];
    }
  }
  return [];
};

const findThreesome = (target: number, numbers: number[]): number[] => {
  for (let i = 0; i < numbers.length; i++) {
    const subTarget = target - numbers[i];
    const subArrayLeft = numbers.slice(0, i);
    const subArrayRight = numbers.slice(i);
    const subArray = subArrayLeft.concat(subArrayRight);

    const twoSome = findTwoSome(subTarget, subArray);
    if (twoSome.length) {
      return [numbers[i], ...twoSome];
    }
  }
  return [];
};

const output = await main();

console.log(output);
