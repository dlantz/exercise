from numpy import prod


def is_op(ch):
    return ch in ["+", "*"]


def execute(op, e1, e2):
    if op == "*":
        return int(e1) * int(e2)
    elif op == "+":
        return int(e1) + int(e2)


def evaluate(expr):
    # move from left to right, parsing operations
    expr_stack = []
    op_stack = []
    parens_stack = []

    sub_expr = ""
    for ch in reversed(expr):
        # print(f'ch: {ch}')
        if ch == ")":
            if len(parens_stack) > 0:
                sub_expr = ch + sub_expr
            parens_stack.append(ch)
        elif ch == "(":
            parens_stack.pop()
            if len(parens_stack) > 0:
                sub_expr = ch + sub_expr
            if len(parens_stack) == 0:
                # print(f'evaluating subexpr: {sub_expr}')
                expr_stack.append(evaluate(sub_expr))
                sub_expr = ""
        elif len(parens_stack) > 0:
            sub_expr = ch + sub_expr
        elif is_op(ch):
            op_stack.append(ch)
        else:
            expr_stack.append(int(ch))

    # print(f'op stack: {op_stack}')
    # print(f'expr stack: {expr_stack}')
    multiply_expr = []
    while len(op_stack) > 0:
        op = op_stack.pop()
        if op == "+":
            e1 = expr_stack.pop()
            e2 = expr_stack.pop()
            result = execute(op, e1, e2)
            expr_stack.append(result)
        else:
            multiply_expr.append(expr_stack.pop())
        # print(f'op stack: {op_stack}')
        # print(f'expr stack: {expr_stack}')
        # print(f'multiply_expr stack: {multiply_expr}')
    if len(expr_stack) == 1:
        multiply_expr.append(expr_stack.pop())

    # print(multiply_expr)
    return prod(multiply_expr)


if __name__ == '__main__':
    file = open("inputs/18.txt", "r")
    rows = file.read().split('\n')
    sum_ = 0
    for row in rows:
        row = row.replace(" ", "")
        # print(evaluate(row))
        sum_ += evaluate(row)
    print(sum_)
