export async function main() {
  const input = await Deno.readTextFile(`./inputs/13.txt`);

  let buses = input
    .split("\n")[1]
    .split(",")
    .map((b) => parseInt(b, 10));

  let time = buses[0],
    step = buses[0];

  // sieve solution. walk through each bus ID,
  // adding the interval until the mod is 0
  // then update the interval to be the multiple
  // of the step and the busId
  for (let n = 1; n < buses.length; n++) {
    const busId = buses[n];
    if (isNaN(busId)) continue;
    while ((time + n) % busId !== 0) {
      time += step;
    }
    step *= busId;
  }
  console.log(time);
}

main();
