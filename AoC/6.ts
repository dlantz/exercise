export async function main() {
  const input = await Deno.readTextFile(`./inputs/${Deno.env.get("N")}.txt`);

  const groups = input.split("\n\n");

  let groupSum = 0;
  groups.forEach((group) => {
    const people = group.split("\n");

    // part 1
    // groupSum += getUniqueYeses(people);

    // part 2
    groupSum += getIntersectionYeses(people);
  });

  console.log(groupSum);
}

const getUniqueYeses = (people: string[]): number => {
  let answerSet = new Set();
  people.forEach((answers: string) => {
    answers.split("").forEach(answerSet.add);
  });
  return answerSet.size;
};

const getIntersectionYeses = (people: string[]): number => {
  let answerSet: Set<string> | undefined;

  people.forEach((answersStr: string) => {
    // no need to make this a set, assuming all answers on the same line are unique
    const answers = answersStr.split("");

    // update master to latest intersection (master ∩ current)
    // if master not initialized (ie. first loop), just set to current
    answerSet = answerSet
      ? new Set(answers.filter((answer) => answerSet?.has(answer)))
      : new Set(answers);
  });

  return answerSet?.size || 0;
};

main();
