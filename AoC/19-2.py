import re


def build_regex(rule_str, rules, parsed_rules):
    expression = ""
    if rule_str in ['a', 'b']:
        return rule_str

    or_exps = [s.strip() for s in rule_str.split('|')]
    or_exps_parsed = []

    for exp in or_exps:
        rule_nums = [int(n) for n in exp.strip().split(' ')]
        sub_expr = ""
        for n in rule_nums:
            regex = build_regex(rules[n], rules, parsed_rules)
            parsed_rules[n] = regex
            sub_expr += regex
        or_exps_parsed.append(sub_expr)

    expression = f'({"|".join(or_exps_parsed)})'
    parsed_rules[rule_str] = expression
    return expression


def check_match(s, parsed_rules):
    # this is completely ridiculous
    # rules[11] = '42 31 | 42 11 31' translates to
    # /(42){n}(31){n}/ where n is > 1
    for n in range(1, 100):
        parsed_rules[11] = f'{parsed_rules[42]}{{{n}}}{parsed_rules[31]}{{{n}}}'
        regex = f'^{parsed_rules[8]}{parsed_rules[11]}$'
        if re.match(regex, s):
            return True


if __name__ == '__main__':
    file = open("inputs/19.txt", "r")
    sections = file.read().split('\n\n')
    rules_raw = sections[0].split('\n')
    messages = sections[1].split('\n')
    rules = {}
    for rule in rules_raw:
        rule_number = int(rule.split(":")[0])
        rule_structure = rule.split(":")[1].strip().replace('"', '')
        rules[rule_number] = rule_structure

    parsed_rules = {}
    regex = f'^{build_regex(rules[0], rules, parsed_rules)}$'

    # rules[8] = '42 | 42 8' translates to /(42)+/
    parsed_rules[8] = f'({parsed_rules[42]})+'

    total_matched = 0
    for message in messages:
        result = check_match(message, parsed_rules)
        # result = re.match(regex, message)
        if result:
            total_matched += 1
    print(total_matched)
