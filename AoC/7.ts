import { readLines } from "./deps.ts";

async function main() {
  // using a map to represent color relationships as a graph
  // key: color, value: all colors that color can be contained by
  const containedBy: { [key: string]: Set<string> } = {};
  for await (const line of readLines(Deno.stdin)) {
    parseRuleIntoMap(line, containedBy);
  }

  const bags = getBags("shiny gold", containedBy);
  console.log(bags.size);
}

const getBags = (
  color: string,
  containedBy: { [key: string]: Set<string> }
) => {
  let colors = new Set();

  const containedByColors = containedBy[color];
  if (containedByColors?.size) {
    colors = new Set(containedByColors);
    containedByColors.forEach(
      (color) =>
        (colors = new Set([...colors, ...getBags(color, containedBy)]))
    );
  }
  return colors;
};

const parseRuleIntoMap = (
  rule: string,
  containedBy: { [key: string]: Set<string> }
): void => {
  const bagColor = rule.split("bags")[0].trim();

  rule
    .split("contain")[1]
    .split(",")
    .forEach((rawRule) => {
      const colorMatch = rawRule.match(/(?<=[0-9]{1} ).*(?= bag)/);
      if (colorMatch) {
        const color = colorMatch[0];
        if (containedBy[color]?.size) {
          containedBy[color].add(bagColor);
        } else {
          containedBy[color] = new Set([bagColor]);
        }
      }
    });
};

main();
