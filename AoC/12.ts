class Ferry {
  x: number;
  y: number;
  degrees: number;
  directions: { [key: number]: number[] } = {
    0: [0, 1],
    90: [1, 0],
    180: [0, -1],
    270: [-1, 0],
  };

  constructor(x: number = 0, y: number = 0, degrees: number = 90) {
    this.x = x;
    this.y = y;
    this.degrees = degrees;
  }

  move(operation: string, argument: number) {
    switch (operation) {
      case "N":
        this.y += argument;
        break;
      case "S":
        this.y -= argument;
        break;
      case "E":
        this.x += argument;
        break;
      case "W":
        this.x -= argument;
        break;
      case "R":
        this.turn(argument);
        break;
      case "L":
        this.turn(360 - argument);
        break;
      case "F":
        const direction = this.directions[this.degrees];
        this.x += direction[0] * argument;
        this.y += direction[1] * argument;
        break;
    }
  }

  turn(degreeChange: number) {
    this.degrees = (this.degrees + degreeChange) % 360;
  }

  manhattanDistance(): number {
    return Math.abs(this.x) + Math.abs(this.y);
  }
}

export async function main() {
  const input = await Deno.readTextFile(`./inputs/12.txt`);

  let operations = input.split("\n");

  const ferry = new Ferry();
  operations.forEach((op) => {
    ferry.move(op.slice(0, 1), parseInt(op.slice(1), 10));
  });
  console.log(ferry.manhattanDistance());
}

main();
