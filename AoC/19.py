import re


def build_regex(rule_str, rules):
    expression = ""
    if rule_str in ['a', 'b']:
        return rule_str

    or_exps = [s.strip() for s in rule_str.split('|')]
    or_exps_parsed = []

    for exp in or_exps:
        rule_nums = [int(n) for n in exp.strip().split(' ')]
        sub_expr = ""
        for n in rule_nums:
            sub_expr += build_regex(rules[n], rules)
        or_exps_parsed.append(sub_expr)

    expression = f'({"|".join(or_exps_parsed)})'
    return expression


if __name__ == '__main__':
    file = open("inputs/19.txt", "r")
    sections = file.read().split('\n\n')
    rules_raw = sections[0].split('\n')
    messages = sections[1].split('\n')
    rules = {}
    for rule in rules_raw:
        rule_number = int(rule.split(":")[0])
        rule_structure = rule.split(":")[1].strip().replace('"', '')
        rules[rule_number] = rule_structure

    regex = f'^{build_regex(rules[0], rules)}$'

    total_matched = 0
    for message in messages:
        result = re.match(regex, message)
        if result:
            total_matched += 1
    print(total_matched)
