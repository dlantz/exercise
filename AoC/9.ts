export class Queue {
  // implement a queue using 2 stacks
  pushStack: number[];
  popStack: number[];

  constructor(initialValues: number[] = []) {
    this.pushStack = [...initialValues];
    this.popStack = [];
  }

  push(v: number) {
    this.pushStack.push(v);
  }

  pop() {
    if (!this.popStack.length) {
      // move all elements from pushstack to popstack in reverse order
      while (this.pushStack.length) {
        const pop = this.pushStack.pop();
        if (pop) this.popStack.push(pop);
      }
    }
    return this.popStack.pop();
  }

  includes(v: number): boolean {
    return this.pushStack.includes(v) || this.popStack.includes(v);
  }

  // TODO this is inefficient garbage
  containsTwoSum(v: number) {
    for (const a of this.pushStack) {
      if (v - a !== a && this.includes(v - a)) {
        return true;
      }
    }
    for (const a of this.popStack) {
      if (v - a !== a && this.includes(v - a)) {
        return true;
      }
    }
    return false;
  }
}

const PREAMBLE_SIZE = 25;
export async function main() {
  const input = await Deno.readTextFile(`./inputs/9.txt`);

  const numbers = input.split("\n").map((n) => parseInt(n, 10));
  const queue = new Queue(numbers.slice(0, PREAMBLE_SIZE));
  const culprit = part1(numbers, queue);
  console.log(`no two sum found for ${culprit}`);

  if (culprit) {
    console.log(part2(numbers, culprit));
  }
}

const part1 = (numbers: number[], queue: Queue) => {
  for (let i = PREAMBLE_SIZE; i < numbers.length; i++) {
    if (queue.containsTwoSum(numbers[i])) {
      queue.push(numbers[i]);
      queue.pop();
    } else {
      return numbers[i];
    }
  }
};

const part2 = (numbers: number[], v: number) => {
  for (let l = 0; l < numbers.length; l++) {
    let runningTotal = 0;
    for (let r = l; r < numbers.length; r++) {
      runningTotal += numbers[r];
      if (runningTotal > v) {
        break;
      } else if (runningTotal === v) {
        const subArray = numbers.slice(l, r + 1);
        return Math.min(...subArray) + Math.max(...subArray);
      }
    }
  }
};

main();
