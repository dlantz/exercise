import { readLines } from "./deps.ts";

interface BagContainRule {
  count: number;
  color: string;
}

export interface BagMap {
  [color: string]: BagContainRule[];
}

async function main() {
  const bags: BagMap = {};
  for await (const line of readLines(Deno.stdin)) {
    parseRuleIntoMap(line, bags);
  }

  console.log(totalBagsRequiredByColor(bags, "shiny gold"));
}

export const totalBagsRequiredByColor = (
  bags: BagMap,
  color: string
): number =>
  bags[color]?.reduce((acc, rule) => {
    return (
      acc +
      rule.count +
      rule.count * totalBagsRequiredByColor(bags, rule.color)
    );
  }, 0) || 0;

const parseRuleIntoMap = (rule: string, bags: BagMap): void => {
  const bagColor = rule.split("bags")[0].trim();

  rule
    .split("contain")[1]
    .split(",")
    .forEach((rawRule) => {
      const colorMatch = rawRule.match(/(?<=[0-9]{1} ).*(?= bag)/);
      const countMatch = rawRule.match(/[0-9]+/);
      if (colorMatch && countMatch) {
        const rule: BagContainRule = {
          color: colorMatch[0],
          count: parseInt(countMatch[0], 10),
        };
        if (bags[bagColor]) {
          bags[bagColor].push(rule);
        } else bags[bagColor] = [rule];
      }
    });
};

main();
