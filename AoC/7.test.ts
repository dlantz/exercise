import { assertEquals } from "https://deno.land/std@0.79.0/testing/asserts.ts";
import { BagMap, totalBagsRequiredByColor } from "./7-2.ts";

const bags: BagMap = {
  "dark olive": [
    { color: "faded blue", count: 3 },
    { color: "dotted black", count: 4 },
  ],
  "faded blue": [{ color: "lalala", count: 5 }],
};
Deno.test({
  name: "totalBagsRequiredByColor",
  fn: () => {
    const totalBags = totalBagsRequiredByColor(bags, "dark olive");
    assertEquals(totalBags, 22);
  },
});
