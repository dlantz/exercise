let rows = [
  ["L", ".", "L", "L", ".", "L", "L", ".", "L", "L"],
  ["L", "L", "L", "L", "L", "L", "L", ".", "L", "L"],
  ["L", ".", "L", ".", "L", ".", ".", "L", ".", "."],
  ["L", "L", "L", "L", ".", "L", "L", ".", "L", "L"],
  ["L", ".", "L", "L", ".", "L", "L", ".", "L", "L"],
  ["L", ".", "L", "L", "L", "L", "L", ".", "L", "L"],
  [".", ".", "L", ".", "L", ".", ".", ".", ".", "."],
  ["L", "L", "L", "L", "L", "L", "L", "L", "L", "L"],
  ["L", ".", "L", "L", "L", "L", "L", "L", ".", "L"],
  ["L", ".", "L", "L", "L", "L", "L", ".", "L", "L"],
];
const app = new Vue({
  el: "#app",
  data: {
    rows,
    done: false,
  },
});

const numAdjacentOccupied = (rowIndex, colIndex, rows) => {
  let count = 0;
  // console.log(`checking for coordinates ${rowIndex}, ${colIndex}`);
  for (let i = rowIndex - 1; i <= rowIndex + 1; i++) {
    if (rows[i] === undefined) continue;
    for (let j = colIndex - 1; j <= colIndex + 1; j++) {
      if (i === rowIndex && j === colIndex) continue;
      if (rows[i][j] === "#") {
        // console.log(`adding count for ${i}, ${j}`);
        count++;
      }
    }
  }
  return count;
};

const runSeatCheck = () => {
  console.log("running seat check");

  let oldState = app.rows.map((row) => [...row]);
  let newState = app.rows.map((row) => [...row]);

  let stateChanged = false;
  for (let r = 0; r < oldState.length; r++) {
    for (let c = 0; c < oldState[0].length; c++) {
      if (
        oldState[r][c] === "L" &&
        numAdjacentOccupied(r, c, oldState) === 0
      ) {
        newState[r][c] = "#";
        stateChanged = true;
      } else if (
        oldState[r][c] === "#" &&
        numAdjacentOccupied(r, c, oldState) >= 4
      ) {
        newState[r][c] = "L";
        stateChanged = true;
      }
    }
  }
  app.rows = newState;
  if (!stateChanged) app.done = true;
};

function runCheck() {
  if (!app.done) {
    setTimeout(() => {
      runSeatCheck();
      runCheck();
    }, 1000);
  }
}

runCheck();
