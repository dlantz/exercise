from numpy import copy


def num_active_neighbors(grid, x, y, z, w):
    # neighbors: 26 other cubes where any of their coordinates differ by at most 1
    w_dim = len(grid)
    z_dim = len(grid[0])
    y_dim = len(grid[0][0])
    x_dim = len(grid[0][0][0])
    active_neighbors = 0
    for w_i in range(w - 1, w + 2):
        if w_i < 0 or w_i == w_dim:
            continue
        for z_i in range(z - 1, z + 2):
            if z_i < 0 or z_i == z_dim:
                continue
            for y_i in range(y - 1, y + 2):
                if y_i < 0 or y_i == y_dim:
                    continue
                for x_i in range(x - 1, x + 2):
                    if x_i < 0 or x_i == x_dim:
                        continue
                    if x_i == x and y_i == y and z_i == z and w_i == w:
                        continue
                    if grid[w_i][z_i][y_i][x_i] == '#':
                        active_neighbors += 1
    return active_neighbors


def pad_x(grid):
    for i, w in enumerate(grid):
        for j, z in enumerate(w):
            for k, y in enumerate(z):
                grid[i][j][k] = ['.'] + y + ['.']


def pad_y(grid):
    x_dim = len(grid[0][0][0])
    for i, w in enumerate(grid):
        for j, z in enumerate(w):
            grid[i][j] = ([['.'] * x_dim]) + grid[i][j] + ([['.'] * x_dim])


def pad_z(grid):
    y_dim = len(grid[0][0])
    x_dim = len(grid[0][0][0])
    for i, w in enumerate(grid):
        grid[i] = ([[['.'] * x_dim] * y_dim]) + \
            grid[i] + ([[['.'] * x_dim] * y_dim])

    return grid


def pad_w(grid):
    z_dim = len(grid[0])
    y_dim = len(grid[0][0])
    x_dim = len(grid[0][0][0])
    grid = ([[[['.'] * x_dim] * y_dim] * z_dim]) + \
        grid + ([[[['.'] * x_dim] * y_dim] * z_dim])
    return grid


def deep_copy(grid):
    new_grid = []
    for w in grid:
        new_w = []
        for z in w:
            new_z = [y.copy() for y in z]
            new_w.append(new_z)
        new_grid.append(new_w)

    return new_grid


def simulate(grid):
    # create a one-cube padding on all six sides
    padded_grid = deep_copy(grid)

    pad_x(padded_grid)
    pad_y(padded_grid)
    padded_grid = pad_z(padded_grid)
    padded_grid = pad_w(padded_grid)

    new_grid = deep_copy(padded_grid)
    for w, hyper in enumerate(padded_grid):
        for z, section in enumerate(hyper):
            for y, row in enumerate(section):
                for x, _ in enumerate(row):
                    n = num_active_neighbors(padded_grid, x, y, z, w)
                    if padded_grid[w][z][y][x] == "#":
                        if n not in [2, 3]:
                            new_grid[w][z][y][x] = "."
                    else:
                        if n == 3:
                            new_grid[w][z][y][x] = "#"

    return new_grid


def p(grid):
    print('GRID')
    for w, hyper in enumerate(grid):
        for z, section in enumerate(hyper):
            print(f'W: {w}, Z: {z}')
            for y in section:
                print(''.join(y))


if __name__ == '__main__':
    file = open("inputs/17.txt", "r")
    rows = file.read().split('\n')
    z_0 = []
    for row in rows:
        z_0.append([ch for ch in row])

    w_0 = [z_0]
    grid = [w_0]
    # p(grid)
    for i in range(6):
        grid = simulate(grid)
        # p(grid)

    total_active = 0
    for w in grid:
        for z in w:
            for y in z:
                total_active += len([cube for cube in y if cube == "#"])
    print(total_active)
