import { readLines } from "./deps.ts";

interface Instruction {
  operation: string;
  argument: number;
}

async function main() {
  const instructions: Instruction[] = [];
  for await (const line of readLines(Deno.stdin)) {
    instructions.push({
      operation: line.split(" ")[0],
      argument: parseInt(line.split(" ")[1], 10),
    });
  }

  console.log(part1(instructions));
  part2(instructions);
}

const part1 = (instructions: Instruction[]): number => {
  let i = 0,
    acc = 0;
  const runs = Array(instructions.length).fill(false);
  while (!runs[i]) {
    runs[i] = true;
    switch (instructions[i].operation) {
      case "nop":
        i++;
        break;
      case "acc":
        acc += instructions[i].argument;
        i++;
        break;
      case "jmp":
        i += instructions[i].argument;
        break;
    }
  }
  return acc;
};

const part2 = (
  instructions: Instruction[],
  runs: boolean[] = Array(instructions.length).fill(false),
  i: number = 0,
  acc: number = 0,
  fixAttempted: boolean = false
) => {
  let instruction = instructions[i];

  while (!runs[i]) {
    runs[i] = true;

    if (!fixAttempted) {
      // swap 'jmp' and 'nop' and recurse with:
      // - fresh copy of runs array,
      // - the new index to start at,
      // - the existing accumulator,
      // - and a flag to ensure that this swap is only run in the root function
      switch (instruction.operation) {
        case "jmp":
          part2(instructions, [...runs], i + 1, acc, true);
          break;
        case "nop":
          part2(instructions, [...runs], i + instruction.argument, acc, true);
          break;
      }
    }

    switch (instruction.operation) {
      case "nop":
        i++;
        break;
      case "acc":
        acc += instruction.argument;
        i++;
        break;
      case "jmp":
        i += instruction.argument;
        break;
    }
    if (i >= instructions.length) {
      // past the end === success
      console.log(`terminal condition reached with accumulator: ${acc}`);
      return;
    }
    instruction = instructions[i];
  }
};

main();
