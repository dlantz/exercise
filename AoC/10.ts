export async function main() {
  const input = await Deno.readTextFile(`./inputs/10.txt`);

  const numbers = input.split("\n").map((n) => parseInt(n, 10));

  numbers.sort((a, b) => a - b);

  // add outlet and device to either end of numbers array
  const allNumbers = [0, ...numbers, numbers[numbers.length - 1] + 3];

  // part1(allNumbers);
  part2(allNumbers);
}
const part1 = (numbers: number[]) => {
  // add outlet and device to either end of numbers array
  const differences: { [key: number]: number } = {};

  for (let i = 0; i < numbers.length - 1; i++) {
    const difference = numbers[i + 1] - numbers[i];
    if (difference > 3) {
      console.log(`no valid adapter in range for ${numbers[i]} `);
      return;
    }
    differences[difference] = differences[difference]
      ? differences[difference] + 1
      : 1;
  }
  console.log(differences[1] * differences[3]);
};

const part2 = (numbers: number[]) => {
  const cache = Array(numbers.length);

  const check = (index: number = numbers.length - 1, count: number = 0) => {
    if (index >= numbers.length) {
      return count;
    } else if (index === numbers.length - 1) {
      return count + 1;
    }
    for (const lookAhead of [1, 2, 3]) {
      if (numbers[index + lookAhead] - numbers[index] <= 3) {
        if (cache[index + lookAhead]) {
          // console.log(`cache hit for ${index + lookAhead}`);
          count += cache[index + lookAhead];
        } else {
          count += check(index + lookAhead);
        }
      }
    }

    return count;
  };
  for (let i = numbers.length - 1; i >= 0; i--) {
    const subCount = check(i);
    // console.log(`setting cache[${i}] to ${subCount}`);
    cache[i] = subCount;
  }

  console.log(cache[0]);
};

main();
