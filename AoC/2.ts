import { readLines } from "./deps.ts";

async function main() {
  let validPasswords = 0;
  for await (const line of readLines(Deno.stdin)) {
    // example input format: 3-5 f: fgfff
    const requirements = line.split(":")[0];

    const range = requirements.split("-").map((n) => parseInt(n));
    const targetLetter = requirements.split(" ")[1];
    const password = line.split(":")[1].trim();

    if (isValidPassword(range, targetLetter, password)) validPasswords++;
  }
  console.log(validPasswords);
}

const isValidPassword = (
  range: number[],
  targetLetter: string,
  password: string
): boolean => {
  const occurrences = (password.match(new RegExp(targetLetter, "g")) || [])
    .length;

  return occurrences >= range[0] && occurrences <= range[1];
};

const isValidPassword2 = (
  indices: number[],
  targetLetter: string,
  password: string
): boolean => {
  // toboggan arrays start at 1, shift the array down
  indices.forEach((_, i) => indices[i]--);

  // XOR - one must be a match and the other must not
  return (
    (password[indices[0]] === targetLetter) !==
    (password[indices[1]] === targetLetter)
  );
};

main();
