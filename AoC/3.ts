import { readLines } from "./deps.ts";

async function main() {
  const patterns: string[] = [];
  for await (const line of readLines(Deno.stdin)) {
    patterns.push(line);
  }

  const slopes = [
    [3, 1],
    // uncomment below for part 2
    // [1, 1],
    // [5, 1],
    // [7, 1],
    // [1, 2],
  ];

  const trees = slopes
    .map((slope) => countTrees(patterns, slope[0], slope[1]))
    .reduce((a, b) => a * b);

  console.log(trees);
}

const countTrees = (
  patterns: string[],
  rightIncrement: number,
  downIncrement: number
): number => {
  let position = 0,
    treesHit = 0;

  for (let i = 0; i < patterns.length; i += downIncrement) {
    const pattern = patterns[i];
    const adjustedPosition = position % pattern.length;

    if (pattern[adjustedPosition] === "#") {
      treesHit++;
    }
    position += rightIncrement;
  }
  return treesHit;
};

main();
