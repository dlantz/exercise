class BitmaskMemory:
    mem = {}

    def save(self, address, value):
        self.current_val = value
        address_bin = bin(address).replace("0b", "")
        address_bin = "0" * (36 - len(address_bin)) + address_bin
        self.write_to_address(address_bin, 0)

    def write_to_address(self, address_bin, index):
        if index > 35:
            self.mem[address_bin] = self.current_val
            return
        if self.mask[index] == "1":
            new_address_bin = address_bin[0:index] + \
                "1" + address_bin[index + 1:]
            self.write_to_address(new_address_bin, index + 1)
        elif self.mask[index] == "0":
            self.write_to_address(address_bin, index + 1)
        elif self.mask[index] == "X":
            address_1 = address_bin[0:index] + "1" + address_bin[index + 1:]
            self.write_to_address(address_1, index + 1)
            address_0 = address_bin[0:index] + "0" + address_bin[index + 1:]
            self.write_to_address(address_0, index + 1)

    def set_mask(self, mask):
        self.mask = mask

    def get_decimal_total(self):
        return sum(self.mem.values())

    def __str__(self):
        return str({'mem': self.mem, 'mask': self.mask})


if __name__ == '__main__':
    file = open("inputs/14.txt", "r")
    mem = BitmaskMemory()
    for line in file.readlines():
        if 'mask' in line:
            mem.set_mask(line.split("=")[1].strip())
        else:
            address = int(line.split("=")[0].replace(
                "mem[", "").replace("]", "").strip())
            value = int(line.split("=")[1].strip())
            mem.save(address, value)

    print(mem.get_decimal_total())
