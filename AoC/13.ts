export async function main() {
  const input = await Deno.readTextFile(`./inputs/13.txt`);

  let minimumLeaveTime = parseInt(input.split("\n")[0], 10);

  let busIds = input
    .split("\n")[1]
    .split(",")
    .map((n) => parseInt(n))
    .filter((n) => !isNaN(n));

  let busId: number | undefined,
    leaveTime = minimumLeaveTime;

  while (true) {
    busId = busIds.find((id) => leaveTime % id === 0);
    if (busId !== undefined) break;
    leaveTime++;
  }

  const minutesToWait = leaveTime - minimumLeaveTime;
  console.log(busId * minutesToWait);
}

main();
