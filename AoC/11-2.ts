export async function main() {
  const input = await Deno.readTextFile(`./inputs/11.txt`);

  let rows = input.split("\n").map((row) => row.split(""));

  const rowsFinal = runSeatChecks(rows);
  const seatsOccupied = rowsFinal
    .map((row) => row.filter((seat) => seat === "#"))
    .reduce((acc, row) => acc + row.length, 0);

  console.log(seatsOccupied);
}

const runSeatChecks = (rows: string[][]): string[][] => {
  let oldState: string[][];
  let newState = rows.map((row) => [...row]);

  let stateChanged = true;
  while (stateChanged) {
    oldState = newState.map((row) => [...row]);
    stateChanged = false;
    for (let r = 0; r < oldState.length; r++) {
      for (let c = 0; c < oldState[0].length; c++) {
        if (
          oldState[r][c] === "L" &&
          numAdjacentOccupied(r, c, oldState) === 0
        ) {
          newState[r][c] = "#";
          stateChanged = true;
        } else if (
          oldState[r][c] === "#" &&
          numAdjacentOccupied(r, c, oldState) >= 5
        ) {
          newState[r][c] = "L";
          stateChanged = true;
        }
      }
    }
  }
  return newState;
};

const numAdjacentOccupied = (
  rowIndex: number,
  colIndex: number,
  rows: string[][]
): number => {
  let count = 0;
  const directions = [
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, -1],
    [0, 1],
    [1, -1],
    [1, 0],
    [1, 1],
  ];

  for (const direction of directions) {
    let x = rowIndex + direction[0],
      y = colIndex + direction[1];

    while (rows[x] && rows[x][y] === ".") {
      x += direction[0];
      y += direction[1];
    }

    if (rows[x] && rows[x][y] === "#") {
      count++;
    }
  }

  return count;
};

main();
