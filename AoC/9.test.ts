import { assertEquals } from "https://deno.land/std@0.79.0/testing/asserts.ts";
import { BagMap, totalBagsRequiredByColor } from "./7-2.ts";
import { Queue } from "./9.ts";

Deno.test({
  name: "queue",
  fn: () => {
    const q = new Queue();
    q.push(1);
    q.push(2);
    assertEquals(q.pop(), 1);
    q.push(3);
    assertEquals(q.pop(), 2);
    q.push(4);
    assertEquals(q.pop(), 3);
    assertEquals(q.pop(), 4);
  },
});
