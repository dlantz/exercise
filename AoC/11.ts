export async function main() {
  const input = await Deno.readTextFile(`./inputs/11.txt`);

  let rows = input.split("\n").map((row) => row.split(""));

  const rowsFinal = runSeatChecks(rows);
  const seatsOccupied = rowsFinal
    .map((row) => row.filter((seat) => seat === "#"))
    .reduce((acc, row) => acc + row.length, 0);

  console.log(seatsOccupied);
}

const runSeatChecks = (rows: string[][]): string[][] => {
  let oldState: string[][];
  let newState = rows.map((row) => [...row]);

  let stateChanged = true;
  while (stateChanged) {
    oldState = newState.map((row) => [...row]);
    stateChanged = false;
    for (let r = 0; r < oldState.length; r++) {
      for (let c = 0; c < oldState[0].length; c++) {
        if (
          oldState[r][c] === "L" &&
          numAdjacentOccupied(r, c, oldState) === 0
        ) {
          newState[r][c] = "#";
          // console.log('setting stateChanged to true')
          stateChanged = true;
        } else if (
          oldState[r][c] === "#" &&
          numAdjacentOccupied(r, c, oldState) >= 4
        ) {
          newState[r][c] = "L";
          stateChanged = true;
        }
      }
    }
  }
  return newState;
};

const numAdjacentOccupied = (
  rowIndex: number,
  colIndex: number,
  rows: string[][]
): number => {
  let count = 0;
  // console.log(`checking for coordinates ${rowIndex}, ${colIndex}`);
  for (let i = rowIndex - 1; i <= rowIndex + 1; i++) {
    if (rows[i] === undefined) continue;
    for (let j = colIndex - 1; j <= colIndex + 1; j++) {
      if (i === rowIndex && j === colIndex) continue;
      if (rows[i][j] === "#") {
        // console.log(`adding count for ${i}, ${j}`);
        count++;
      }
    }
  }
  // console.log(`total count: ${count}`);
  return count;
};

main();
