def in_range(value, ranges):
    # print('in_range function:')
    # print(value)
    # print(ranges)
    # print(any(value >= range_[0] and value <= range_[1] for range_ in ranges))
    return any(value >= range_[0] and value <= range_[1] for range_ in ranges)


if __name__ == '__main__':
    file = open("inputs/16.txt", "r")
    sections = file.read().split('\n\n')

    rule_section = sections[0]
    my_ticket_section = sections[1]
    nearby_tickets_section = sections[2]

    rules = {}
    all_ranges = []
    for rule in rule_section.split('\n'):
        name = rule.split(':')[0]
        ranges_raw = rule.split(':')[1].split(' or ')
        ranges = []
        for r in ranges_raw:
            ranges.append((int(r.split('-')[0].strip()),
                           int(r.split('-')[1].strip())))
            all_ranges.append((int(r.split('-')[0].strip()),
                               int(r.split('-')[1].strip())))
        rules[name] = ranges

    valid_tickets = []
    for nearby_ticket in nearby_tickets_section.split('\n')[1:]:
        valid = True
        for value in [int(n) for n in nearby_ticket.split(',')]:
            if not any(value >= range_[0] and value <= range_[1] for range_ in all_ranges):
                valid = False
        if valid:
            valid_tickets.append(nearby_ticket)

    # determine rule position possibilities
    rule_position_possibilities = {}
    for index in range(len(valid_tickets[0].split(','))):
        for rule in rules:
            if all(in_range(int(ticket.split(',')[index]), rules[rule]) for ticket in valid_tickets):
                if rule in rule_position_possibilities:
                    rule_position_possibilities[rule].append(index)
                else:
                    rule_position_possibilities[rule] = [index]

    # reconcile possibilities
    positions_final = {}
    taken = set()
    while len(positions_final) < len(rules):
        for rule in rule_position_possibilities:
            filtered = [
                i for i in rule_position_possibilities[rule] if i not in taken]
            if len(filtered) == 1:
                positions_final[rule] = filtered[0]
                taken.add(filtered[0])

    my_ticket = [int(v) for v in my_ticket_section.split('\n')[1].split(',')]

    output = 1
    for rule in [rule for rule in positions_final if 'departure' in rule]:
        i = positions_final[rule]
        output *= my_ticket[i]

    print(output)
