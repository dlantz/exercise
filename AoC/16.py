if __name__ == '__main__':
    file = open("inputs/16.txt", "r")
    sections = file.read().split('\n\n')

    rule_section = sections[0]
    my_ticket_section = sections[1]
    nearby_tickets_section = sections[2]

    ranges = []
    for rule in rule_section.split('\n'):
        name = rule.split(':')[0]
        ranges_raw = rule.split(':')[1].split(' or ')
        for r in ranges_raw:
            ranges.append((int(r.split('-')[0].strip()),
                           int(r.split('-')[1].strip())))

    invalid_values = []
    for nearby_ticket in nearby_tickets_section.split('\n')[1:]:
        for value in [int(n) for n in nearby_ticket.split(',')]:
            if not any(value >= range_[0] and value <= range_[1] for range_ in ranges):
                invalid_values.append(value)

    print(sum(invalid_values))
