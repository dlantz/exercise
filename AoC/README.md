# Advent of Code 2020 (Deno + TS)

(I switched to python on day 14 because of a new job)

### Pre-requisites:

Deno: https://deno.land/#installation

### Running the code:

```bash
$ # Line by line
$ deno run {day}.ts < inputs/{day}.txt
$ # Reading whole file at once
$ make run N={day}
```
