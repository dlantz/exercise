import { readLines } from "./deps.ts";

class Seat {
  row: number;
  column: number;
  id: number;

  constructor(row: number, column: number) {
    this.row = row;
    this.column = column;
    this.id = row * 8 + column;
  }
}

async function main() {
  const seatsHash: { [key: number]: Seat } = {};
  for await (const line of readLines(Deno.stdin)) {
    const seat = parseSeat(line);
    seatsHash[seat.id] = seat;
  }
  // part 1
  console.log(getMaxSeatId(seatsHash));

  // part 2:
  console.log(findMissingSeatId(seatsHash));
}

const getMaxSeatId = (seatsHash: { [key: number]: Seat }): number => {
  return Math.max(
    ...Object.keys(seatsHash).map((seatId) => parseInt(seatId, 10))
  );
};

const findMissingSeatId = (seatsHash: { [key: number]: Seat }): number => {
  // find seat ID that doesn't have a neighbor (this may trigger a false positive for largest seatID)
  const seatId = Object.keys(seatsHash).find(
    (seatId) => !seatsHash[parseInt(seatId, 10) + 1]
  );
  return seatId ? parseInt(seatId, 10) + 1 : -1;
};

const parseSeat = (pass: string): Seat => {
  // Convert to binary represenation, and use parseInt
  // B = 1, F = 0, R = 1, L = 0

  const rowStringBinary = pass
    .slice(0, 7)
    .replaceAll("B", "1")
    .replaceAll("F", "0");
  const row = parseInt(rowStringBinary, 2);

  const colStringBinary = pass
    .slice(7)
    .replaceAll("R", "1")
    .replaceAll("L", "0");
  const col = parseInt(colStringBinary, 2);

  return new Seat(row, col);
};

main();
