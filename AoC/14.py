
class BitmaskMemory:
    mem = {}

    def save(self, address, value):
        binString = bin(value)
        binString = binString.replace("0b", "")
        # pad to 36
        binString = "0" * (36 - len(binString)) + binString
        masked = ''.join(map(self.apply_mask, zip(self.mask, binString)))
        self.mem[address] = masked

    def set_mask(self, mask):
        self.mask = mask

    def apply_mask(self, pair):
        if pair[0] == "X":
            return pair[1]
        else:
            return pair[0]

    def get_decimal_total(self):
        return sum(int(value, 2) for value in self.mem.values())

    def __str__(self):
        return str({'mem': self.mem, 'mask': self.mask})


if __name__ == '__main__':
    file = open("inputs/14.txt", "r")
    mem = BitmaskMemory()
    for line in file.read().splitlines():
        if 'mask' in line:
            mem.set_mask(line.split("=")[1].strip())
        else:
            address = int(line.split("=")[0].replace(
                "mem[", "").replace("]", "").strip())
            value = int(line.split("=")[1].strip())
            mem.save(address, value)

    print(mem.get_decimal_total())
