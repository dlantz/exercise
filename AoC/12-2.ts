class Ferry {
  x: number;
  y: number;
  waypoint: number[];

  constructor(x: number = 0, y: number = 0, waypoint: number[] = [10, 1]) {
    this.x = x;
    this.y = y;
    this.waypoint = waypoint;
  }

  execute(operation: string, argument: number) {
    switch (operation) {
      case "N":
        this.waypoint[1] += argument;
        break;
      case "S":
        this.waypoint[1] -= argument;
        break;
      case "E":
        this.waypoint[0] += argument;
        break;
      case "W":
        this.waypoint[0] -= argument;
        break;
      case "R":
        this.rotateWaypoint(argument);
        break;
      case "L":
        this.rotateWaypoint(360 - argument);
        break;
      case "F":
        this.x += this.waypoint[0] * argument;
        this.y += this.waypoint[1] * argument;
        break;
    }
  }

  rotateWaypoint(degreeChange: number) {
    // assumes all degree inputs are multiples of 90
    for (let rotations = degreeChange / 90; rotations > 0; rotations--) {
      this.waypoint = [this.waypoint[1], -this.waypoint[0]];
    }
  }

  manhattanDistance(): number {
    return Math.abs(this.x) + Math.abs(this.y);
  }
}

export async function main() {
  const input = await Deno.readTextFile(`./inputs/12.txt`);

  let operations = input.split("\n");

  const ferry = new Ferry();
  operations.forEach((op) => {
    ferry.execute(op.slice(0, 1), parseInt(op.slice(1), 10));
  });

  console.log(ferry.manhattanDistance());
}

main();
