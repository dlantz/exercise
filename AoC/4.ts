interface Passport {
  [key: string]: string;
}

export async function main() {
  const input = await Deno.readTextFile(`./inputs/${Deno.env.get("N")}.txt`);

  const passportsRaw = input.split("\n\n");

  const passports = passportsRaw.map((raw) => {
    const passport: Passport = {};
    raw.split("\n").forEach((line) => {
      line.split(" ").forEach((field) => {
        const kv = field.split(":");
        passport[kv[0]] = kv[1];
      });
    });
    return passport;
  });
  const numValid = passports.filter((passport) => isValidPart2(passport))
    .length;

  console.log(numValid);
}

const isValid: ValidationFn = (passport) => {
  const requiredFields = ["hcl", "iyr", "eyr", "ecl", "pid", "byr", "hgt"];
  return requiredFields.every((field) => passport[field]);
};

const isValidPart2: ValidationFn = (passport) => {
  return [
    birthYearValid,
    expirationYearValid,
    issueYearValid,
    heightValid,
    hairColorValid,
    eyeColorValid,
    passportIdValid,
  ].every((fn) => fn(passport));
};

const numStrInRange = (
  numStr: string,
  atLeast: number,
  atMost: number
): boolean => {
  const num = parseInt(numStr);
  return num >= atLeast && num <= atMost;
};

type ValidationFn = (passport: Passport) => boolean;

const birthYearValid: ValidationFn = (passport) =>
  numStrInRange(passport["byr"], 1920, 2002);

const issueYearValid: ValidationFn = (passport) =>
  numStrInRange(passport["iyr"], 2010, 2020);

const expirationYearValid: ValidationFn = (passport) =>
  numStrInRange(passport["eyr"], 2020, 2030);

const heightValid: ValidationFn = (passport) => {
  const heightStr = passport["hgt"];

  if (heightStr?.split("cm").length === 2)
    return numStrInRange(heightStr.split("cm")[0], 150, 193);

  if (heightStr?.split("in").length === 2)
    return numStrInRange(heightStr.split("in")[0], 59, 76);

  return false;
};

const hairColorValid: ValidationFn = (passport) => {
  return !!passport["hcl"]?.match(/^\#[a-f0-9]{6}$/);
};

const eyeColorValid: ValidationFn = (passport) => {
  const validColors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
  return validColors.includes(passport["ecl"]);
};

const passportIdValid: ValidationFn = (passport) => {
  return !!passport["pid"]?.match(/^[0-9]{9}$/);
};

main();
