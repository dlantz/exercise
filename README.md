# Code Exercises

A home for programming exercises. Used to learn new languages, hone old algorithms, and enjoy the craft of programming in general.

The code in this repo is not trying to be perfect. I like to leave an archaeological trail of trivial solutions, commented sections, and other ill-fated digressions. It's something I can look back on as a way to remember my thought process (like a journal in that way), and add new, better implementations instead of erasing everything for the sake of an invincible appearance.
