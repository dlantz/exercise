# https://www.hackerrank.com/challenges/luck-balance/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms

from functools import reduce  # only in Python 3

# Complete the luckBalance function below.


def luckBalance(k, contests):
    maxLuck = 0
    nonImportant = list(filter(lambda contest: contest[1] == 0, contests))
    maxLuck += sum([x[0] for x in nonImportant])

    important = list(filter(lambda contest: contest[1] == 1, contests))
    importantSorted = sorted(
        important, key=lambda contest: contest[0], reverse=True)

    contestsToLose = importantSorted[:k]
    contestsToWin = importantSorted[k:]

    maxLuck += sum([x[0] for x in contestsToLose])
    maxLuck -= sum([x[0] for x in contestsToWin])

    return maxLuck


if __name__ == '__main__':

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    contests = []

    for _ in range(n):
        contests.append(list(map(int, input().rstrip().split())))

    result = luckBalance(k, contests)

    print(result)
