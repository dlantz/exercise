# https://www.hackerrank.com/challenges/ctci-queue-using-two-stacks/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=stacks-queues&h_r=next-challenge&h_v=zen


class MyQueue(object):
    def __init__(self):
        self.enqueueStack = []
        self.dequeueStack = []

    def peek(self):
        if len(self.dequeueStack) == 0:
            return self.enqueueStack[0]
        else:
            return self.dequeueStack[-1]

    def pop(self):
        if len(self.dequeueStack) == 0:
            self.moveItems()
        self.dequeueStack.pop()

    def put(self, value):
        self.enqueueStack.append(value)

    def moveItems(self):
        while len(self.enqueueStack) > 0:
            self.dequeueStack.append(self.enqueueStack.pop())


queue = MyQueue()
t = int(input())
for line in range(t):
    values = map(int, input().split())
    values = list(values)
    if values[0] == 1:
        queue.put(values[1])
    elif values[0] == 2:
        queue.pop()
    else:
        print(queue.peek())
