# https://www.hackerrank.com/challenges/balanced-brackets/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=stacks-queues

BRACKETS = {'[': ']', '{': '}', '(': ')'}


def isBalanced(s):
    stack = []
    for index in range(0, len(s)):
        if isClosingBracket(s[index]):
            try:
                matchedBracket = stack.pop()
                if not bracketMatch(matchedBracket, s[index]):
                    return 'NO'
            except IndexError:
                return 'NO'
        else:
            stack.append(s[index])
    if len(stack) == 0:
        return "YES"
    return "NO"


def isClosingBracket(c):
    return c in BRACKETS.values()


def bracketMatch(b1, b2):
    return BRACKETS[b1] == b2


if __name__ == '__main__':

    t = int(input())

    for t_itr in range(t):
        s = input()

        result = isBalanced(s)
        print(result)
