# https://www.hackerrank.com/challenges/greedy-florist/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=greedy-algorithms

import math
import os
import random
import re
import sys

# Complete the getMinimumCost function below.


def getMinimumCost(numFriends, flowers):
    # get number of multipliers
    if numFriends >= len(flowers):
        return sum(flowers)

    total = 0
    multiplier = 1
    flowers.sort(reverse=True)
    index = 0

    while index < len(flowers):
        if index != 0 and index % numFriends == 0:
            multiplier += 1
        total += multiplier * flowers[index]
        index += 1
    return total


if __name__ == '__main__':

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    c = list(map(int, input().rstrip().split()))

    minimumCost = getMinimumCost(k, c)
    print(minimumCost)
