# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        ans = self.findDepth(root, 0)
        return ans

    def findDepth(self, node: TreeNode, counter: int) -> int:
        if node.left.val != None:
            self.findDepth(node.left, counter + 1)
        if node.right.val != None:
            self.findDepth(node.right, counter + 1)
        return counter
