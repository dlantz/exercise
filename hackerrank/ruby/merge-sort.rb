def merge_sort(a)
  merge_sort_recursive(a, 0, a.length - 1)
end

def merge_sort_recursive(a, left, right)
  return [a[left]] if left >= right

  middle = (left + right) / 2
  # merge left
  left_array =  merge_sort_recursive(a, left, (left + right) / 2)

  # merge right
  right_array = merge_sort_recursive(a, middle + 1, right)

  left_ptr = 0
  right_ptr = 0
  output = []
  output_index = 0
  while (left_ptr < left_array.length) || (right_ptr < right_array.length)
    if left_ptr == left_array.length # done placing elements from left hand side
      output[output_index] = right_array[right_ptr]
      right_ptr += 1
      output_index += 1
      next
    end
    if right_ptr == right_array.length # done placing elements from right hand side
      output[output_index] = left_array[left_ptr]
      left_ptr += 1
      output_index += 1
      next
    end
    if left_array[left_ptr] <= right_array[right_ptr]
      output[output_index] = left_array[left_ptr]
      left_ptr += 1
    else
      output[output_index] = right_array[right_ptr]
      right_ptr += 1
    end
    output_index += 1
  end
  output
end

a = gets.rstrip.split(' ').map(&:to_i)

puts merge_sort a
