# https://www.hackerrank.com/challenges/alternating-characters

require 'json'
require 'stringio'

# Complete the alternatingCharacters function below.
def alternatingCharacters(s)
  index = 0
  deletions = 0
  while index < s.length - 1
    j = 1
    while s[index] == s[index + j]
      deletions += 1
      j += 1
    end
    index += j
  end
  deletions
end

# fptr = File.open(ENV['OUTPUT_PATH'], 'w')

q = gets.to_i

q.times do |_q_itr|
  s = gets.to_s.rstrip

  result = alternatingCharacters s

  puts result
  # fptr.write result
  # fptr.write "\n"
end

# fptr.close
