# Currently only works on arrays of positive numbers

def counting_sort(a)
  counts = []
  a.each do |value|
    counts[value].nil? ? counts[value] = 1 : counts[value] += 1
  end
  # modify count array such that each element stores the sum of previous counts
  counts.each_index do |index|
    next if index.zero?

    counts[index] = (counts[index] || 0) + (counts[index - 1] || 0)
  end

  # shift counts array one to the right (if you want to handle zeroes)
  counts.unshift(0)
  counts.pop

  puts "counts: #{counts}"
  output = []
  # compose output array
  a.each do |value|
    # take each value from original array, and get the value at its index in the count array
    # set index in the output array to that value, and increment the count array
    output_index = counts[value]
    output[output_index] = value
    counts[value] += 1
  end
  puts output
end

a = gets.rstrip.split(' ').map(&:to_i)

counting_sort a
