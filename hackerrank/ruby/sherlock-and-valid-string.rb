# https://www.hackerrank.com/challenges/sherlock-and-valid-string
N_BYTES = [42].pack('i').size
N_BITS = N_BYTES * 16
MAX = 2**(N_BITS - 2) - 1
MIN = -MAX - 1

def isValid_1(s)
  char_counts = {}
  maximum = MIN
  minimum = MAX
  (0..s.length - 1).each do |index|
    character = s[index]
    char_counts[character] = char_counts[character].nil? ? 1 : char_counts[character] + 1
    minimum = [minimum, char_counts[character]].min
    maximum = [maximum, char_counts[character]].max
  end
  return :NO if (minimum > 1) && ((maximum - minimum) > 1)

  # check if there is a whole number average
  if (s.length % char_counts.length).zero?
    average = s.length / char_counts.length
    # check two indices. if they are both equal to the average, we're gucci
    if (char_counts[char_counts.keys[0]] == average) || (char_counts[char_counts.keys[1]] == average)
      return :YES
    else
      return :NO
    end
  elsif ((s.length - 1) % char_counts.length).zero?
    average = (s.length - 1) / char_counts.length
    # check two indices. if they are both equal to the average, we're gucci
    if (char_counts[char_counts.keys[0]] == average) || (char_counts[char_counts.keys[1]] == average)
      return :YES
    else
      return :NO
    end
  end
  :NO
end

def isValid(s)
  char_counts = {}
  (0..s.length - 1).each do |index|
    character = s[index]
    char_counts[character] = char_counts[character].nil? ? 1 : char_counts[character] + 1
  end
  # good ol iteration - there cannot be more than 2 keys each with different numbers
  frequencies = {}
  char_counts.values.each do |value|
    frequencies[value] = frequencies[value].nil? ? 1 : frequencies[value] + 1
  end
  puts frequencies
  puts "frequencies.values.include? 1: #{frequencies.values.include? 1}"
  puts "(frequencies.keys[0] - frequencies.keys[1]).abs < 2: #{(frequencies.keys[0] - frequencies.keys[1]).abs < 2}"
  case frequencies.values.length
  when 1
    :YES
  when 2
    return :YES if frequencies[1] == 1

    if (frequencies.values.include? 1) &&
       ((frequencies.keys[0] - frequencies.keys[1]).abs < 2)
      :YES
    else
      :NO
    end
  else
    :NO
  end
end
# check if there is a whole number average
#   if (s.length % char_counts.length).zero?
#     average = s.length / char_counts.length
#     # check two indices. if they are both equal to the average, we're gucci
#     if (char_counts[char_counts.keys[0]] == average) || (char_counts[char_counts.keys[1]] == average)
#       return :YES
#     else
#       return :NO
#     end
#   elsif ((s.length - 1) % char_counts.length).zero?
#     average = (s.length - 1) / char_counts.length
#     # check two indices. if they are both equal to the average, we're gucci
#     if (char_counts[char_counts.keys[0]] == average) || (char_counts[char_counts.keys[1]] == average)
#       return :YES
#     else
#       return :NO
#     end
#   end
#   :NO
# end

s = gets.to_s.rstrip

result = isValid s

puts result
