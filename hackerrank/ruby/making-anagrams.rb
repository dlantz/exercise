# https://www.hackerrank.com/challenges/ctci-making-anagrams

require 'json'
require 'stringio'

# Complete the makeAnagram function below.
def makeAnagram(a, b)
  letter_counts = {}
  a.split('').each do |letter|
    letter_counts[letter] = letter_counts[letter].nil? ? 1 : letter_counts[letter] + 1
  end

  b.split('').each do |letter|
    letter_counts[letter] = letter_counts[letter].nil? ? -1 : letter_counts[letter] - 1
  end

  # delete all letters that aren't cancelled out at this point
  letter_counts.values.reduce(0) do |total, current_val|
    total + current_val.abs
  end

  # puts "letter_counts: #{letter_counts}"
end

# fptr = File.open(ENV['OUTPUT_PATH'], 'w')

a = gets.to_s.rstrip

b = gets.to_s.rstrip

res = makeAnagram a, b
puts res

# fptr.write res
# fptr.write "\n"

# fptr.close()
