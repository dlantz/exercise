# https://www.hackerrank.com/challenges/ctci-bubble-sort/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting

require 'json'
require 'stringio'

# Complete the countSwaps function below.
def countSwaps(a)
  swaps = 0
  i = 0
  while i < a.length
    j = 0
    while j < a.length - 1
      if a[j] > a[j + 1]
        a[j], a[j + 1] = a[j + 1], a[j]
        swaps += 1
      end
      j += 1
    end
    i += 1
  end
  puts "Array is sorted in #{swaps} swaps."
  puts "First Element: #{a[0]}"
  puts "Last Element: #{a[-1]}"
end

gets.to_i

a = gets.rstrip.split(' ').map(&:to_i)

countSwaps a
