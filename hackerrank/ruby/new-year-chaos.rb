# https://www.hackerrank.com/challenges/new-year-chaos

require 'json'
require 'stringio'

# Complete the minimumBribes function below.
def minimumBribes(q)
  bribes = 0
  q.each_with_index do |person, index|
    # in starting state, the index of a person is person - 1
    # if a person is more than two spots 'ahead' of their place in line, it is too chaotic
    # AKA if the difference between index + 1 and the person at that index is greater than two,
    # it is too chaotic

    # person at index 0 cannot be more than 3
    # person at index 1 cannot be more than 4
    # ...
    if (person - (index + 1)) > 2
      puts 'Too chaotic'
      return
    end

    # how many people bribed each index?
    # check back two spaces from person's original index.
    # if either of those numbers is bigger than person,
    # then person must have been bribed by them
    person_index = person - 1
    p = [0, person_index - 2].max
    while p < index
      bribes += 1 if q[p] > person
      p += 1
    end
  end
  puts bribes
end

t = gets.to_i

t.times do |_t_itr|
  n = gets.to_i

  q = gets.rstrip.split(' ').map(&:to_i)

  minimumBribes q
end
