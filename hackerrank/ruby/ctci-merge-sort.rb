# https://www.hackerrank.com/challenges/ctci-merge-sort

require 'json'
require 'stringio'

# Complete the countInversions function below.
def countInversions(a)
  _, swaps = merge_sort_swaps_recursive(a, 0, a.length - 1)
  swaps
end

def merge_sort_swaps_recursive(a, left, right)
  return [a[left]], 0 if left >= right

  middle = (left + right) / 2
  # merge left
  left_array, left_swaps =  merge_sort_swaps_recursive(a, left, (left + right) / 2)

  # merge right
  right_array, right_swaps = merge_sort_swaps_recursive(a, middle + 1, right)

  # puts "left: #{left_array}"
  # puts "right: #{right_array}"
  # left_ptr = 0
  # right_ptr = 0
  output = []
  # output_index = 0

  swaps = left_swaps + right_swaps

  while left_array.length.positive? && right_array.length.positive?
    if left_array[0] <= right_array[0]
      output.push(left_array.shift)
    else
      # if value at front of right array is smaller,
      # that value must traverse the length of the smaller array in swaps
      swaps += left_array.length
      output.push(right_array.shift)
    end

  end
  output += if left_array.length.zero?
              right_array
            else
              left_array
              # swaps += left_array.length
            end
  [output, swaps]
end

# fptr = File.open(ENV['OUTPUT_PATH'], 'w')

t = gets.to_i

t.times do |_t_itr|
  n = gets.to_i

  arr = gets.rstrip.split(' ').map(&:to_i)

  result = countInversions arr

  puts result
  # fptr.write result
  # fptr.write "\n"
end

# fptr.close
