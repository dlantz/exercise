# https://www.hackerrank.com/challenges/fraudulent-activity-notifications

require 'json'
require 'stringio'

def activityNotifications_trivial(expenditure, trailing_days)
  # puts 'expenditure', expenditure
  # puts 'trailing_days', trailing_days
  notifications = 0
  i = 0
  while i + trailing_days < expenditure.length
    days_expenditure = expenditure[i + trailing_days]
    # puts 'days expenditure', days_expenditure
    slice_of_concern = expenditure[i, i + trailing_days]
    # puts 'slice of concern', slice_of_concern
    median = get_median(slice_of_concern)
    # puts 'median', median
    notifications += 1 if days_expenditure >= median * 2
    i += 1
  end
  notifications
end

def get_median(a)
  a = a.sort
  return a[a.length / 2] if a.length.odd?

  (a[(a.length / 2) - 1] + a[a.length / 2]) / 2.0
end

def activityNotifications_withCountSort(expenditure, trailing_days)
  notifications = 0

  middle_index2 = trailing_days / 2
  middle_index1 = trailing_days.even? ? middle_index2 - 1 : middle_index2

  # puts "middle index 1: #{middle_index1}"
  # puts "middle index 2: #{middle_index2}"

  # initialize counts
  counts = [0] * 201

  # add first few expenditures to the counts array
  # and to the queue
  expenditure_queue = []
  (0..trailing_days - 1).each do |i|
    counts[expenditure[i]] += 1
    expenditure_queue.unshift expenditure[i]
  end

  (trailing_days..expenditure.length - 1).each do |i|
    days_expenditure = expenditure[i]
    # puts "days expenditure: #{days_expenditure}"
    # puts "expenditure queue: #{expenditure_queue}"
    # puts "counts: #{counts}"

    # find the median in the counts array
    # median = 0
    if middle_index1 == middle_index2
      median = get_median_from_counts(counts, middle_index1)
      # puts "Odd length: median is #{median}"
    else
      m1, m2 = get_medians_from_counts(counts, middle_index1, middle_index2)
      # puts "even length: medians are #{m1} and #{m2}"
      median = (m1 + m2) / 2.0
    end

    notifications += 1 if days_expenditure >= median * 2

    # rotate counts array:
    expenditure_queue.unshift(days_expenditure)
    counts[days_expenditure] += 1
    popped = expenditure_queue.pop
    counts[popped] -= 1
  end
  notifications
end

def get_median_from_counts(counts, target_index)
  accumulator = 0
  count_index = 0
  while accumulator <= target_index
    accumulator += counts[count_index]
    count_index += 1
  end
  count_index - 1
end

# in case the array is even, need to grab both medians from either side of the center line
def get_medians_from_counts(counts, smaller_index, larger_index)
  accumulator = 0
  count_index = 0
  smaller_index_track = 0
  while accumulator <= larger_index
    accumulator += counts[count_index]
    count_index += 1
    smaller_index_track = count_index if accumulator <= smaller_index
  end
  [smaller_index_track, count_index - 1]
end
nd = gets.rstrip.split

n = nd[0].to_i

d = nd[1].to_i

expenditure = gets.rstrip.split(' ').map(&:to_i)

puts activityNotifications_withCountSort expenditure, d
