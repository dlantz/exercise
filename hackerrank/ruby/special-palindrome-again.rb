# https://www.hackerrank.com/challenges/special-palindrome-again
# note: I know these aren't actual palindromes, the naming convention had too much momentum by the time I realized
require 'json'
require 'stringio'

# Complete the substrCount function below.
def substrCount(n, s)
  total = 0
  (0..n - 1).each do |index|
    total += num_even_palindromes(s, index)
    total += num_odd_palindromes(s, index)
  end
  total + n
end

def num_even_palindromes(s, index)
  total = 0
  i1 = index
  i2 = index + 1
  char_to_match = s[i1]
  while in_bounds(i1, i2, s.length) && (s[i1] == char_to_match) && (s[i2] == char_to_match)
    total += 1
    i1 -= 1
    i2 += 1
  end
  total
end

def num_odd_palindromes(s, index)
  total = 0
  i1 = index - 1
  i2 = index + 1
  char_to_match = s[i1]
  while in_bounds(i1, i2, s.length) && (s[i1] == char_to_match) && (s[i2] == char_to_match)
    total += 1
    i1 -= 1
    i2 += 1
  end
  total
end

def in_bounds(lower, upper, length)
  (lower >= 0) && (upper <= (length - 1))
end

n = gets.to_i

s = gets.to_s.rstrip

result = substrCount n, s

puts result
