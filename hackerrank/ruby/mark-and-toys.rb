# https://www.hackerrank.com/challenges/mark-and-toys

# !/bin/ruby

require 'json'
require 'stringio'

# Complexity: O(nlogn) for sort AND O(n) for iterating
# so O(nlogn + n)
def maximumToys(prices, k)
  sorted = prices.sort

  index = 0
  num_toys = 0
  while sorted[index] <= k
    k -= sorted[index]
    num_toys += 1
    index += 1
  end
  num_toys
end

# fptr = File.open(ENV['OUTPUT_PATH'], 'w')

nk = gets.rstrip.split

n = nk[0].to_i

k = nk[1].to_i

prices = gets.rstrip.split(' ').map(&:to_i)

result = maximumToys prices, k

puts result
# fptr.write result
# fptr.write "\n"

# fptr.close
