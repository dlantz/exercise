/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
const rotate = function (matrix) {
  const firstRow = matrix[0];

  let topBound = 0;
  let leftBound = 0;
  let rightBound = matrix[0].length - 1;
  let bottomBound = matrix.length - 1;

  while (topBound < bottomBound) {
    let i = topBound;
    for (let j = i; j < rightBound; j++) {
      console.log("i: ", i);
      console.log("j: ", j);

      // top to right
      [rightI, rightJ] = topToRight(i, j, rightBound);
      console.log("right coordinates: ", rightI, rightJ);
      let bottom = matrix[rightI][rightJ];
      matrix[rightI][rightJ] = matrix[i][j];

      // right to bottom
      [bottomI, bottomJ] = rightToBottom(rightI, rightJ, leftBound);
      console.log("bottom coordinates: ", bottomI, bottomJ);
      let left = matrix[bottomI][bottomJ];
      matrix[bottomI][bottomJ] = bottom;

      // bottom to left
      [leftI, leftJ] = bottomToLeft(bottomI, bottomJ, leftBound);
      console.log("left coordinates: ", leftI, leftJ);
      let top = matrix[leftI][leftJ];
      matrix[leftI][leftJ] = left;

      // left to top
      [topI, topJ] = leftToTop(leftI, leftJ, leftBound, rightBound);
      console.log("top coordinates: ", topI, topJ);
      matrix[topI][topJ] = top;
    }

    topBound += 1;
    leftBound += 1;
    rightBound -= 1;
    bottomBound -= 1;
  }
  matrix.forEach((row) => console.log(row));
};

// 0,0, =>  0, 3
// 0, 1 => 1, 3
// 0, 2 => 2, 3
// 0, 3 => 3, 3
const topToRight = (i, j, rightBound) => {
  return [j, rightBound];
};

// 0, 3 => 3, 3
// 1, 3 => 3, 2
// 2, 3 => 3, 1
// 3, 3 => 3, 0
// 1, 2 => 2, 2
const rightToBottom = (i, j, leftBound) => {
  iAdjusted = i - leftBound;
  return [j, j - iAdjusted];
};

// 3, 3 => 3, 0
// 3, 2 => 2, 0
// 3, 1 => 1, 0
// 3, 0 => 0, 0
// 2, 1 => 1, 1
// 2, 2 =>
const bottomToLeft = (i, j, leftBound) => {
  return [j, leftBound];
};

// 3, 0 => 0, 0
// 2, 0 => 0, 1
// 1, 0 => 0, 2
// 0, 0 => 0, 3
//2, 1 => 1, 1
const leftToTop = (i, j, leftBound, rightBound) => {
  const iAdjusted = i - leftBound;
  return [j, rightBound - iAdjusted];
};
