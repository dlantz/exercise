/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function (nums) {
  const maxRobberies = {}
  return robRecursive(0, 0)
  
  function robRecursive(i, total) {
      if (i >= nums.length) {
          return total
      }
      
      // rob current, skip two ahead
      const twoSkips = nums[i] + maxRobberies[i + 2] ? maxRobberies[i + 2] robRecursive(i + 2, total + nums[i])
      
      // do not rob current, skip one ahead
      const oneSkip = maxRobberies[i + 1] ? maxRobberies[i + 1] : robRecursive(i + 1, total)
      
      const maxValue = Math.max(twoSkips, oneSkip)
      maxRobberies[i] = maxValue
      console.log(maxRobberies)
      return maxValue
  }
};
