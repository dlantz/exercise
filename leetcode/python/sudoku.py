
class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        # for each number, go in all directions and validate there are no duplicates
        for rowIndex in range(len(board)):
            row = board[rowIndex]
            for colIndex in range(len(row)):
                cell = row[colIndex]
                if cell == ".":
                    continue
                num = int(cell)
                if self.isValidRow(num, row):
                    return False
                return False if not self.isValidColumn(num, colIndex, board)
                return False if not self.isValidBox(num, colIndex, rowIndex, board)
        return True

    def isValidRow(self, num, row):
        return row.count(num) == 1

    def isValidColumn(self, num, colIndex, board):
        occurrenceCount = 0
        for row in board:
            if row[colIndex] == num:
                occurrenceCount += 1
                if occurrenceCount > 1:
                    return False
        return True

    def isValidBox(self, num, colIndex, rowIndex, board):
        xBoxCoord = colIndex // 3
        yBoxCoord = rowIndex // 3
        occurrenceCount = 0
        for i in range(yBoxCoord, yBoxCoord + 3):
            for j in range(xBoxCoord, xBoxCoord + 3):
                if board[i][j] == num:
                    occurrenceCount += 1
                    if occurrenceCount > 1:
                        return False
        return True
