# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?

def is_prime(number)
  current = 2
  factors = [1, number]
  while (factors.length == 2) && (current < number)
    factors << current if (number % current).zero?
    current += 1
  end
  factors.length == 2
end

# ugly first attempt - this one turned the processor orange
def largest_prime_factor_1(number)
  primes = []
  current = 1
  while current < number
    if (number % current).zero? # we only care if it's a factor
      modded = false
      # check if it's modded out by existing prime
      primes.each do |prime|
        modded = (current % prime).zero?
        break if modded
      end
      primes << current unless modded
    end
    current += 1
  end
  puts primes
  primes.max
end

puts "largest prime factor: #{largest_prime_factor_1(600_851_475_143)}"
