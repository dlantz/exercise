# Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:

# 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

# By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.

def sum_even_fibonacci(max, term1, term2, sum)
  return sum if term2 >= max

  sum += term2 if term2.even?
  sum_even_fibonacci(max, term2, term1 + term2, sum)
end

puts "result: #{sum_even_fibonacci(4_000_000, 1, 2, 0)}"
