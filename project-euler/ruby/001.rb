# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.

MAX = 1000
current_three = 3
current_five = 5
sum = 0

while current_three < MAX
  sum += current_three

  # skip if already a multiple
  sum += current_five if current_five < MAX && current_five % 3 != 0

  current_three += 3
  current_five += 5
end

puts "sum for #{MAX}: #{sum}"
